var CHECKBOX_FILTER_CLASS = {
	/**
     * @param element : .element
     * @return object
     */
	getChecked : (element) => {
	  var elements = {};
	  const checkbox = element+":checked";
	  const selected = element+" option:selected";
	  
	  jQuery.each(jQuery(checkbox), function(){            
		var name = jQuery(this).attr('name');
		
		if( typeof( elements[name] ) == 'undefined' ){
		  elements[name] = [];
		}
		elements[name].push(jQuery(this).val());
	  });
	  
	  jQuery.each(jQuery(selected), function(){            
		var name = jQuery(this).parent().attr('name');
		
		if( typeof( elements[name] ) == 'undefined' ){
		  elements[name] = [];
		}
		elements[name].push(jQuery(this).val());
	  });
	  
	  return elements;
    },
    /**
     * @param element : .element
     * @return object
     */
	getSelected : (element) => {
	  var elements = {};
	  const selected = element+" option:selected";
	  
	  jQuery.each(jQuery(selected), function(){            
		var name = jQuery(this).parent().attr('name');
		
		if( typeof( elements[name] ) == 'undefined' ){
		  elements[name] = [];
		}
		elements[name].push(jQuery(this).val());
	  });
	  
	  
	  return elements;
    },
    
    getUrlParameter : (name) => {
		name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
		var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
		var results = regex.exec(location.search);
		return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
	},

	pushState : (query_string) => {
		var d = new Date();
		//window.history.replaceState('ok', 'no', "?test=56&y=8");
		//window.location.replace("test=56&y=8");
		window.history.pushState('Page', 'Title', query_string);
	},
	ajaxContent : (ajaxurl, callback, data) => {
		//console.log(callback);
		//console.log(data);
		jQuery.ajax({
            url : ajaxurl,
            type : 'post',
            data : {action : 'woo_getWoocommerceProductsList', args : data},
            success : function( response ) {
                
            },beforeSend : function(){
				
			},error : function(){
				
			}
        });
	}
}

/**
 * @examples:
 */
/*
$(document).ready(function(){
  $(document).on('click','.element',function(){
		console.log(CHECKBOX_FILTER_CLASS.getChecked('.element'));	
  });
});
*/

var URL_GENERATOR_CLASS = {
	/**
	* @param url : http://url.com?t=3&y=6
	* @return object | false
	*/
	getParams : function (url) {
		try{
			var params = {};
			var parser = document.createElement('a');
			var href = parser.href = url;

			if( url.indexOf('?') <= -1 ){
			  throw "url don't have query string param!";
			}

			var href_array = href.split('?');

			var query = parser.search.substring(1);
			var vars = query.split('&');

			if( vars.length <= 0 ){
			  throw "url don't have query string:value combination!";
			}

			for (var i = 0; i < vars.length; i++) {
			  var pair = vars[i].split('=');
			  params[pair[0]] = decodeURIComponent(pair[1]);
			}

			return {url:href_array[0],params:params};

		  }catch(err){
			return false;
		  }
		},
	  /**
	   * @param url : http://url.com?t=3&y=6
	   * @param key : t
	   * @param data: string | []
	   * @return string
	   */
	  setParams : function (url,key,data) {
			var params = URL_GENERATOR_CLASS.getParams(url);
			params.params[key] = data;
			
			var query_params = [];
			for( var v in params.params ){
				query_params.push(v+'='+params.params[v]);
			}
			
			var site_url = params.url+'?'+query_params.join('&');
			
			return site_url;
		},
		
		getUrlDefaultValues : () => {
			const get_default_values = URL_GENERATOR_CLASS.getParams(location.href);
			
			if(get_default_values === false){
				return false;
			}
			
			var params_array = [];
			if(get_default_values != false){
				for(var i in get_default_values.params){
					var params = get_default_values.params[i].split(',');
					
					for(var j = 0; j <= params.length; j++){
						if(typeof(params[j]) != 'undefined'){
							params_array.push(params[j]);
						}
					}
				}
				
				return params_array;
			}
		},
	  
	  /**
	   * @param data : json object
	   * @param query: true | false for '?'
	   * @return string
	   */
		setMultipleParams : ( data, query ) => {
			let url = '?';
			let params = [];
		  
			for(let v in data){
				params.push(v+'='+data[v]);
			}
		  
			if(query === true){
				return url+''+params.join('&');
			}
		  
			return params.join('&');
		},
	  
	};

//console.log(URL_GENERATOR_CLASS.setMultipleParams(json, true));

//console.log( URL_GENERATOR_CLASS.setParams(url, 'filter_manufacturer', ['test','test2']) );
//console.log( URL_GENERATOR_CLASS.getParams(url) );
