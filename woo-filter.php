<?php
/**
 * @package woo filter
 */
/*
Plugin Name: Woo Filter
Plugin URI: https://akismet.com/
Description: Woo Filter
Version: 4.0.3
Author: Automattic
Author URI: https://automattic.com/wordpress-plugins/
License: GPLv2 or later
Text Domain: Woo Filter
*/

class Taxonomy_Terms_Html_Scripts{
	
	public function enqueue_script() {		
		wp_enqueue_script( 'script-woo-filter', plugin_dir_url( __FILE__ ) . 'js/script.js', '1.0.0', false );
	}
}

add_action('wp_enqueue_scripts', array( new Taxonomy_Terms_Html_Scripts(), 'enqueue_script'));

class Taxonomy_Terms_Html{
	
	protected $terms = [];
	
	/**
	 * @param $taxonomy | taxonomy name
	 * @param $args | array
	 * @return object
	 */
	public function __construct($taxonomy, $args = []){
		try{
			$terms = get_terms( $taxonomy, $args);
			$this->terms['taxonomy'] = $taxonomy;
			
			foreach( $terms as $term ){
				$this->terms['terms'][$term->term_id] = $term;
			}
			
			return $this;
		}catch(Exception $ex){
			$this->terms['terms'] = [];
			return $this;
		}
	}
	
	/**
	 * @param $heading | custom name
	 * @param $args | array
	 * @return html | select list
	 */
	public function generateSelectOptions($heading, $args = []){
		$classes = implode(' ',[ isset($args['selector']) ? $args['selector'] : '', 'tax_'.$this->terms['taxonomy'] ]);
		$html = '<div class="container_'.$this->terms['taxonomy'].'"><h2>'.$heading.'</h2>';
		$html .= '<select class="'.$classes.' element" id="tax_'.$this->terms['taxonomy'].'" name="'.$this->terms['taxonomy'].'">';
		$html .= '<option value="">--</option>';	
		
		if(sizeof($this->terms['terms']) >0 ){
			foreach($this->terms['terms'] as $term){
				$html .= '<option class="term_'.$term->term_id.'" class="term_'.$term->term_id.'" value="'.$term->term_id.'">'.$term->name.'</option>';	
			}
		}
		$html .= '</select></div>';
		
		return $html;
	}
	
	/**
	 * @param $heading | custom name
	 * @param $args | array
	 * @return radio list
	 */
	public function generateRadioButtons($heading, $args = []){
		$classes = implode(' ',[ isset($args['selector']) ? $args['selector'] : '' ]);
		$html = '<div class="container_'.$this->terms['taxonomy'].'"><h2>'.$heading.'</h2>';
		$html .= '<ul class="" id="tax_'.$this->terms['taxonomy'].'">';	
		
		if(sizeof($this->terms['terms']) >0 ){
			foreach($this->terms['terms'] as $term){
				$html .= '<li><input type="radio" name="'.$this->terms['taxonomy'].'" class="'.$classes.' element term_'.$term->term_id.'" value="'.$term->term_id.'"/>'.$term->name.'</li>';	
			}
		}
		$html .= '</ul></div>';
		
		return $html;
	}
	
	/**
	 * @param $heading | custom name
	 * @param $args | array
	 * @return checkbox list
	 */
	public function generateCheckbox($heading, $args = []){
		$classes = implode(' ',[ isset($args['selector']) ? $args['selector'] : '' ]);
		$html = '<div class="container_'.$this->terms['taxonomy'].'"><h2>'.$heading.'</h2>';
		$html .= '<ul class="" id="tax_'.$this->terms['taxonomy'].'">';	
		
		if(sizeof($this->terms['terms']) >0 ){		
			foreach($this->terms['terms'] as $term){
				$html .= '<li><input type="checkbox" class="'.$classes.' element term_'.$term->term_id.'" name="'.$this->terms['taxonomy'].'" value="'.$term->term_id.'"/>'.$term->name.'</li>';	
			}
		}
		$html .= '</ul></div>';
		
		return $html;
	}
	
}

/**
 * @description Query Handler class
 */ 
class WOO_FILTER_TERMS_QUERIES{
	
	/**
	 * @description : get product list.
	 */  
	public function getWoocommerceProductsList(){
		$wr = apply_filters('woo_get_products_query', $wr);
		echo 56; exit;
	}
	
}

add_action( 'wp_ajax_nopriv_woo_getWoocommerceProductsList', array(new WOO_FILTER_TERMS_QUERIES, 'getWoocommerceProductsList') );
add_action( 'wp_ajax_woo_getWoocommerceProductsList', array(new WOO_FILTER_TERMS_QUERIES(), 'getWoocommerceProductsList') );


function WOO_FILTER_TERMS_FUNC( $atts ) {
	$t = new Taxonomy_Terms_Html($atts['taxonomy']);
	
	switch($atts['type']){
		case 'radio':
			return $t->generateRadioButtons($atts['taxonomy']);
				break;
		
		case 'checkbox':
			return $t->generateCheckbox($atts['taxonomy']);
				break;
			
		case 'select':
			return $t->generateSelectOptions($atts['taxonomy']);
				break;
	}
}
add_shortcode( 'WOO_FILTER_TERMS', 'WOO_FILTER_TERMS_FUNC' );

/**
 * @tutorial: 
 * $taxonomies = [
		[
			'taxonomy' => 'category',
			'type' => 'checkbox',
			'classes' => '',
			'id' => '',
			'heading' => 'Category Title'
		]
	];
	$instance = [
		'selector' => '.element',
		'pagination' => '',
		'number_of_pages' => '',
		'sort_by' => '',
	];
	
	$r = base64_encode(json_encode($taxonomies));
	$i = base64_encode(json_encode($instance)); 
	
	echo do_shortcode('[WOO_FILTER_TEXONOMIES taxonomies="'.$r.'" instance="'.$i.'"]');
 */ 
function WOO_FILTER_TEXONOMIES_FUNC( $atts ) {
	try{
		$taxonomies = json_decode(base64_decode($atts['taxonomies']));
		$instance = json_decode(base64_decode($atts['instance']));
		
		$html = '';
		
		foreach($taxonomies as $taxonomy){
		
		$t = new Taxonomy_Terms_Html($taxonomy->taxonomy);
		switch($taxonomy->type){
			case 'radio':
				$html .= $t->generateRadioButtons($taxonomy->heading, ['selector' => $instance->selector]);
					break;
			
			case 'checkbox':
				$html .= $t->generateCheckbox($taxonomy->heading, ['selector' => $instance->selector]);
					break;
				
			case 'select':
				$html .= $t->generateSelectOptions($taxonomy->heading, ['selector' => $instance->selector]);
					break;
			}
		}
		
		// set js.
		/**
		 * @description: get chekbox values.
		 */
		$html .= "<script type='text/javascript'>jQuery(document).ready(function(){
				
				/**
			  	 * @description: set default values onload.
				 */	
				if(URL_GENERATOR_CLASS.getUrlDefaultValues() != false){
					for( var i = 0; i <= URL_GENERATOR_CLASS.getUrlDefaultValues().length; i ++){
						var term_id = URL_GENERATOR_CLASS.getUrlDefaultValues();
						if( typeof(term_id[i]) != 'undefined' ){
							var term = '.term_'+term_id[i];
							
							jQuery(term).prop('checked', true);
						
						}
					}
				}
				
				jQuery(document).on('click','.".$instance->selector."',function(){
					/**
					 * @description: get chekbox values.
					 */
					const get_checked = CHECKBOX_FILTER_CLASS.getChecked('.".$instance->selector."');
					console.log(get_checked);
					
					/**
					 * @description: change browser url without refresh.
					 */			
					const query_url = URL_GENERATOR_CLASS.setMultipleParams(get_checked,true); 		
					//console.log(query_url);
					CHECKBOX_FILTER_CLASS.pushState(query_url);
					
					/**
					 * @description: change browser url without refresh.
					 */
					var ajaxurl = '".admin_url('admin-ajax.php')."';
					CHECKBOX_FILTER_CLASS.ajaxContent(ajaxurl, 'woo_getWoocommerceProductsList', {terms:get_checked});	
			  });
			});</script>";
		
		return $html;
		
		}catch(exception $ex){
		
	}
}
add_shortcode( 'WOO_FILTER_TEXONOMIES', 'WOO_FILTER_TEXONOMIES_FUNC' );

function WOO_FILTER_JS_EXECUTE_FUNC( $atts ){
	$html = "<script type='text/javascript'>jQuery(document).ready(function(){
			  jQuery(document).on('click','".$atts['class']."',function(){
					console.log(CHECKBOX_FILTER_CLASS.getChecked('".$atts['class']."'));	
			  });
			});</script>";
	$html .= "<script type='text/javascript'>jQuery(document).ready(function(){
			  jQuery(document).on('click','".$atts['class']."',function(){
					CHECKBOX_FILTER_CLASS.pushState('".$atts['class']."');	
			  });
			});</script>";
	$html .= "<script type='text/javascript'>jQuery(document).ready(function(){
			  jQuery(document).on('click','".$atts['class']."',function(){
					CHECKBOX_FILTER_CLASS.ajaxContent('{}');	
			  });
			});</script>";		
	echo $html;		
}
add_shortcode( 'WOO_FILTER_JS_EXECUTE', 'WOO_FILTER_JS_EXECUTE_FUNC' );

?>
